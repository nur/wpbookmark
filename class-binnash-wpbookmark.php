<?php
/**
 * Description of class-binnash-wpbookmark
 *
 * @author nur
 */
require_once ('lang/eng.php');
require_once ('class-binnash-wpbookmark-config.php');
require_once ('class.binnash-wpbookmark-list.php');
if (!class_exists('WPBookmark')) {
class WPBookmark {
    function WPBookmark(){
        $this->__construct();
    }
    
    function __construct() {
        add_action('init', array(&$this, 'loadLibrary'));
        add_action("admin_init", array(&$this, 'adminInit'));
        add_action('admin_menu', array(&$this, 'adminMenu'));
        add_filter('the_content', array(&$this, 'bookmarkHandler'));
		add_action('wp_head', array(&$this,'wpHead'));
        add_action('wp_footer', array(&$this, 'addJsCode'));
        add_action('wp_ajax_binnash_bookmark', array(&$this, 'doBookmark'));
        add_action('wp_ajax_binnash_bookmark_list', array(&$this, 'doBookmarkList'));
        add_shortcode('binnash_bookmark_list', array(&$this, 'bookmarkListShorcodeHandler'));
        add_action("wpbookmark_mail_delivery_event", array(&$this, 'wpbookmark_cronjob'));
        add_action("admin_notices", array(&$this, 'admin_notices'));
        add_action('load-settings_page_wp_bookmark_manage', function(){
		    wp_enqueue_script('jquery.tools',WPBOOKMARK_URL.'/js/jquery.tools18.min.js');
		    wp_enqueue_style('jquery.tools.dateinput',WPBOOKMARK_URL.'/css/jquery.tools.dateinput.css');              
        });
//        WPBookmark::activate();
    }
    function admin_notices(){
        $msg = get_transient('wpbookmark_message');
        if(empty($msg)) return;            
        ?>
        <div class="updated">
            <p><?php echo $msg; ?></p>
        </div>
        <?php
    }  
    function wpbookmark_cronjob(){
        $conf = WPBookmarkConfig::getInstance();
        $queue = $conf->queue;
        if(!empty($queue)){
            $entry = array_pop($queue);
            $conf->updateConfig('queue', $queue)->save();
            if(empty($entry)) return;
            $this->send_mail($entry['user_ids'], $entry);        
        }                
    }
    function adminInit(){
        if (isset($_POST['send_mail_button'])){
            $post_id = isset($_POST['post_id'])?$_POST['post_id'] : "";
            $conf = WPBookmarkConfig::getInstance(); 
            global $wpdb;
            if(!empty($post_id)){  
                $user_ids = $wpdb->get_col("select user_id from 
                " . $conf->bookmarks_tbl . "  WHERE post_id IN (". $post_id . ")");            

                if(isset($_POST['enqueue'])){
                    $queue = (array)$conf->queue;
                    $entry = array('user_ids' => $user_ids,
                                   'subject'  => $_POST['subject'],
                                   'body'     => $_POST['body'],
                                   'signature'=> $_POST['signature'],
                                   'address1' => $_POST['address1'],
                                   'address2' => $_POST['address2'],    
                                   'city'     => $_POST['city'],
                                   'state'    => $_POST['state'],
                                   'zip'      => $_POST['zip']   
                             );
                    array_unshift($queue, $entry);
                    $conf->updateConfig('queue', $queue)->save();
                    set_transient("wpbookmark_message", "Emails have been queued for delivery.", 10);
                }
                else{
                    $this->send_mail($user_ids, $_POST);                    
                }    
            }
            else{
                
            }
            $conf->updateConfig('mail_body', $_POST['body'])
                     ->updateConfig('mail_subject', $_POST['subject'])
                     ->updateConfig('mail_signature', $_POST['signature'])
                     ->updateConfig('mail_address1', $_POST['address1'])
                     ->updateConfig('mail_address2', $_POST['address2'])
                     ->updateConfig('mail_city', $_POST['city'])
                     ->updateConfig('mail_state', $_POST['state'])
                     ->updateConfig('mail_zip', $_POST['zip'])
                     ->updateConfig('queue', array())
                     ->save();                   
            wp_redirect('admin.php?page=wp_bookmark_manage');
        }
    }
	public function send_mail($member_ids, $mail_data){
		global $wpdb;
		/**
				Send only plain text. Attachments and HTML content raise flags with content filters.
				Set the message header: “Precedence: bulk”
				You must set a subject, body, from address, and reply-to address (not having reply-to was my problem		
		*/
		$keys = array('user_login','user_firstname','user_lastname',
					  'user_email','display_name','user_url');
		
		if(empty($member_ids)) return;
		$query = implode(',', $member_ids);
		$query  = "SELECT ID, user_login,user_firstname,                    
                    user_lastname,user_email,display_name,user_url FROM " . $wpdb->users . " WHERE ID in ( " . $query . ")";

		$results = $wpdb->get_results($query,ARRAY_A);
		$subject = stripslashes(html_entity_decode($mail_data['subject'], ENT_NOQUOTES, 'UTF-8'));
		$address = $mail_data['address1'] . "\n" . $mail_data['address2'] . "\n" .
		           $mail_data['city'] . "\n" . $mail_data['state'] . "-" . $mail_data['zip']; 
		$from 	 = get_option('admin_email');
		$headers    = array
		(
			'MIME-Version: 1.0',
			'Content-Type: text/html; charset="UTF-8";',
			'Content-Transfer-Encoding: 7bit',
			'Date: ' . date('r', $_SERVER['REQUEST_TIME']),
			'Message-ID: <' . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>',
			'From: ' . $from,
			'Precedence: bulk',
			'Reply-To: ' . $from,
			'Return-Path: ' . $from,
			'X-Mailer: PHP v' . phpversion(),
			'X-Originating-IP: ' . $_SERVER['SERVER_ADDR'],
		);
        if(empty($results)){
            set_transient("wpbookmark_message", "No email was sent!.", 10);
            return;
        }
		foreach($results as $result){
			$search  = array();
			$replace = array();
			foreach($keys as $key){
				$search[]  = '{' . $key .'}';
				$replace[] = $result[$key]; 
			}
			$body = str_replace($search, $replace, $mail_data['body']);
			$body = stripslashes(html_entity_decode($body, ENT_NOQUOTES, 'UTF-8'));
			if(!empty($mail_data['signature']))$body = $body . "\n\n" . $mail_data['signature'];
			if(!empty($address)) $body = $body . "\n" . $address;
			wp_mail($result['email'], $subject, $body, $headers);
		}
        set_transient("wpbookmark_message", "Emails have been sent!.", 10);        
	}
	function wpHead(){
		$conf = WPBookmarkConfig::getInstance(); 
		$css = '<style type="text/css">
						.binnash-bookmark-button-override{
							float: '.$conf->alignment.';
							margin-'.$conf->alignment.': '.$conf->left_right_adjust.'px;
							margin-top: '.$conf->top_bottom_adjust.'px;
						}
				   </style>	
				  ';
		echo $css;
	}
    function doBookmarkList(){        
        if(is_user_logged_in()){
            global $wpdb;
            $userInfo = wp_get_current_user();
            $conf = WPBookmarkConfig::getInstance();            
            $page = isset($_GET['page'])?$_GET['page']-1:0;
            if(isset($_GET['remove'])&&!empty($_GET['remove'])){
                $wpdb->query("DELETE FROM ".$conf->bookmarks_tbl .
                        " WHERE user_id = " . $userInfo->ID .
                        " AND post_id IN (" . 
                        strip_tags($_GET['remove']) . ")");
            }
            
            $limit =  isset($conf->bbookmark_items_per_page)?$conf->bbookmark_items_per_page:10;
            $start = $page*$limit;
            $query = "SELECT COUNT(" . $wpdb->posts . " .ID) FROM  " .
                    $wpdb->posts ." LEFT JOIN " . $conf->bookmarks_tbl . " ON (" . 
                    $wpdb->posts . " .ID = ". $conf->bookmarks_tbl . ".post_id".                    
                    ") WHERE " .$conf->bookmarks_tbl . ".user_id=" .$userInfo->ID;            
            $row_count = $wpdb->get_var($query);             
            $pages = ceil($row_count/$limit);                                    
            $query = "SELECT " . $wpdb->posts . " .ID, post_title,post_type,date FROM  " .
                    $wpdb->posts ." LEFT JOIN " . $conf->bookmarks_tbl . " ON (" . 
                    $wpdb->posts . " .ID = ". $conf->bookmarks_tbl . ".post_id".                    
                    ") WHERE " .$conf->bookmarks_tbl . ".user_id=" .$userInfo->ID .
                    " ORDER BY " . $wpdb->posts . " .post_title ASC LIMIT " . $start . ', ' . $limit;

            $result = $wpdb->get_results($query, OBJECT);
            ob_start();
            include_once('bookmark_list.php');
            $output = ob_get_contents();            
            ob_end_clean();             
            echo $output;
            exit();
        }
    }
    function bookmarkListShorcodeHandler($attrs, $contents, $codes){
        if(is_user_logged_in()){
            $ajaxurl = admin_url('admin-ajax.php');
            ob_start();
            include_once('bookmark_list_container.php');
            $output = ob_get_contents();
            ob_end_clean();
            return $output;
        }
        return BBOOKMARK_LOGIN_REQUEST;
    }
    function doBookmark(){
        if(is_user_logged_in()){
            global $wpdb;
            $conf = WPBookmarkConfig::getInstance();
            $userInfo = wp_get_current_user();
            if('add' == $_GET['op']){
                $wpdb->hide_errors();    
                $result = $wpdb->insert( 
                    $conf->bookmarks_tbl, 
                    array( 
                        'user_id' => $userInfo->ID, 
                        'post_id' => $_GET['id'], 
                        'type' => $_GET['type'],
                        'date' => date('Y-m-d')
                    ), 
                    array( 
                        '%d', 
                        '%d',
                        '%s',
                        '%s'
                    ) 
                );        
                if($result){
                    $removeTitle = BBOOKMARK_REMOVE;
                    $a1 = '<span class="c"><b>&radic;</b></span><br/>
                                <span class="t">'.BBOOKMARK_BOOKMARK.'</span>
                           ';                
                    $a1 = array('title'=>$removeTitle,'op'=>'remove', 'text'=>$a1);
                    echo json_encode(array('status'=>1,'msg'=>$a1));
                }
                else{
                    $a1 = "Failed!";
                    echo json_encode(array('status'=>0,'msg'=>$a1));
                }
                exit(0);        
            }
            else if ("remove" == $_GET['op']){
                $query = 'DELETE FROM ' . $conf->bookmarks_tbl .
                         ' WHERE post_id =' . $_GET['id'] . ' AND '.
                         ' user_id = ' .$userInfo->ID . ' AND '.
                         ' type = \'post\'';
                $result = $wpdb->query($query);
                if($result){
                    $addTitle = BBOOKMARK_ADD_FAV;
                    $a1 = '<span class="c"><b>+</b></span><br/>
                           <span class="t">'.BBOOKMARK_BOOKMARK.'</span>';                  
                    $a1 = array('title'=>$addTitle,'op'=>'add', 'text'=>$a1);
                    echo json_encode(array('status'=>1,'msg'=>$a1));
                }
                else{
                    $a1 = "Failed.";   
                     echo json_encode(array('status'=>0,'msg'=>$a1));
                }
                exit(0);
            }
        }
    }
    function addJsCode(){
        include_once ('bookmark_button_js.php');
    }
    function bookmarkHandler($content){
        global $post;
        if(is_category()) return $content;
        if(is_feed()) return $content;
        if(is_search()) return $content; 
        $conf = WPBookmarkConfig::getInstance();
        $disabled = isset($conf->disable_bookmarks)?$conf->disable_bookmarks:array();
        $disabled = isset($disabled['post'])?$disabled['post']:array();
        if(in_array($post->ID, $disabled)) return $content;         
        if(is_user_logged_in()){
            $userInfo = wp_get_current_user();
            global $wpdb;
            $query = 'SELECT * FROM ' . $conf->bookmarks_tbl .
                     ' WHERE user_id = ' . $userInfo->ID .
                     ' AND post_id = ' . $post->ID.
                     ' AND type = \'post\'';
            $results = $wpdb->get_results($query);            
            $addTitle = BBOOKMARK_ADD_FAV;
            $removeTitle = BBOOKMARK_REMOVE;
            $link  =  $post->ID;
            if(!empty($results)){
                $a1 = '<a title="' . $removeTitle . '" op="remove" target="_parent" href="'.$link.'" class="count">
                            <span class="c"><b>&radic;</b></span><br/>
                            <span class="t">'.BBOOKMARK_BOOKMARK.'</span>
                        </a>';                
            }
            else{
                $a1 = '<a title="' . $addTitle . '" op="add" target="_parent" href="'.$link.'" class="count">
                            <span class="c"><b>+</b></span><br/>
                            <span class="t">'.BBOOKMARK_BOOKMARK.'</span>
                        </a>';
            }   
        }
        else{
            $addTitle = BBOOKMARK_LOGIN_TO_BOOKMARK;
            $link  = "";
            $a1 = '<a title="' . $addTitle . '"  class="count">
                        <span class="c"><b style="color:red;">x</b></span><br/>
                        <span class="t">'.BBOOKMARK_BOOKMARK.'</span>
                    </a>';
        }	
        $button = '<div class="binnash-bookmark-button binnash-bookmark-button-override binnash-bookmark-button-'.$conf->color.'">
                       <div  class="binnashbookmarkbutton">
                       '.$a1.'				
                </div>
            </div>
            ';
        return $button . $content;        
    }
    
    function loadLibrary(){
        wp_enqueue_script('jquery');
        if(!is_admin()){
             wp_enqueue_style('wpbbookmark', WPBOOKMARK_URL . '/css/wpbbookmark.css'); 
             wp_enqueue_style('jquery.paginateN-1.0', WPBOOKMARK_URL . '/css/jquery.paginateN-1.0.css'); 
             wp_enqueue_script('jquery.paginateN-1.0', WPBOOKMARK_URL . '/js/jquery.paginateN-1.0.js');
        }
        if(is_admin()&&isset($_GET['page'])&&($_GET['page']=='wp_bookmark_manage')){                
            wp_enqueue_style('wpbbookmark-admin', WPBOOKMARK_URL . '/css/wpbbookmark-admin.css'); 
            wp_enqueue_style('jquery.paginateN-1.0', WPBOOKMARK_URL . '/css/jquery.paginateN-1.0.css'); 
            wp_enqueue_script('jquery.paginateN-1.0', WPBOOKMARK_URL . '/js/jquery.paginateN-1.0.js');
        }
    }
    
    function adminMenu(){
        add_options_page('WPBookmark', 'WPBookmark', 'manage_options', "wp_bookmark_manage",array(&$this,'wpBookmarkHook'));
        add_submenu_page(__FILE__, __("WPBookmarK", 'wp_bookmark'), __("WPBookmark", 'wp_boomark'), 'add_users', 'wp_bookmark_manage', array(&$this,'wpBookmarkHook'));        
    }
    
    function wpBookmarkHook(){
        $menuInfo = $this->drawMenu();
        $page_content = 'Content Not Found.';
        $menu = $menuInfo['menu'];
        switch ($menuInfo['current_menu_id']){
            case 'settings':
                $page_content = $this->settingsPage();
                break;
            default:
                $page_content = $this->managePage();
                break;
        }
        include_once ('bookmark_manage.php');
    }
    function managePage(){
        ob_start();
        $action = isset($_GET['action'])? $_GET['action']:"";
        switch($action){
            case 'bulk_mail':
            $settings = WPBookmarkConfig::getInstance();
            $post_id = isset($_REQUEST['post_ids'])? implode(',',$_REQUEST['post_ids']):"";
            include_once('email.php');                
            break;    
            case 'send_mail':
            $settings = WPBookmarkConfig::getInstance();
            $post_id = isset($_REQUEST['post_id'])? absint($_REQUEST['post_id']):""; 
            include_once('email.php');                
            break;    
            default:
            $list = new BinnashWpBookmarkList();
            $list->prepare_items();
            include_once('manage_page.php');
            break;        
        }
        $content = ob_get_contents();
        ob_end_clean();			
        return $content;			                
    }
    function settingsPage(){        
        global $wpdb; 
        $conf = WPBookmarkConfig::getInstance();
        if(isset($_POST['op_edit_settngs'])){
            unset($_POST['op_edit_settngs']);				
            $fields = $_POST;
            if(!isset($_POST['disable_bookmarks']))
                $_POST['disable_bookmarks'] = array();
            $conf->updateConfig($_POST)->save();
            $d = isset($_POST['disable_bookmarks'])?
                $_POST['disable_bookmarks']:array();
            if(!empty($d)){
                $idStrings = implode(',',$_POST['disable_bookmarks']);
                $query = "DELETE FROM " . $conf->bookmarks_tbl . 
                        " WHERE post_id in (" . $idStrings . ')';
                $wpdb->query($query);
            }
        }
        $fields['disable_bookmarks'] = $conf->disable_bookmarks;
        $fields['bbookmark_items_per_page'] =$conf->bbookmark_items_per_page; 
        $query  = "SELECT ID,post_title, post_type FROM $wpdb->posts ";
        $query .= " WHERE post_status = 'publish'";
		/*if(isset($_POST['search-submit'])){
			$query .=" AND post_title LIKE '%" . strip_tags($_POST['search_mark']) . "%'";
		}*/
        $all_posts = $wpdb->get_results($query,ARRAY_A);
        ob_start();
        include_once('settings_page.php');
        $content = ob_get_contents();
        ob_end_clean();			
        return $content;			        
    }
    function drawMenu(){
        $menuInfo = WPBookmarkConfig::menu();
        $menuIds = array_keys($menuInfo);
        $requestedMenu = isset($_GET['menu_id'])? $_GET['menu_id']: 'manage';        
        $currentMenu = in_array($requestedMenu, $menuIds)? $requestedMenu: 'manage';
        $menu = '<ul class="binnash-bookmark-submenu">';
        foreach($menuInfo as $key=>$value){
            $menu .= "<li ";			     
            if($currentMenu ==$key) $menu .= 'class="current"';
            $menu .= '><a href="'.$value['link'].'">'.$value['title'].'</a></li>';
        }   				
        $menu .='</ul>';
        return array('menu'=>$menu,'current_menu_id'=>$currentMenu);
    }
    
    static function activate(){ 
        $conf = WPBookmarkConfig::getInstance();
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = "CREATE TABLE " . $conf->bookmarks_tbl . " (
        id INT NOT NULL AUTO_INCREMENT,
        post_id int NOT NULL ,
        user_id int NOT NULL ,
        date   date NOT NULL ,
        type  varchar(20) NOT NULL DEFAULT 'post',
        unique index(post_id, user_id, type),
		PRIMARY KEY  (id)
        ) AUTO_INCREMENT=1";
        dbDelta($sql);
        if(!isset($conf->disable_bookmarks))
            $conf->updateConfig(array(
                'disable_bookmarks'=>array(),
                'bbookmark_items_per_page'=>10,
				''
                ))->save();
		if(!isset($conf->alignment))$conf->updateConfig('alignment','right')->save();
		if(!isset($conf->left_right_adjust))$conf->updateConfig('left_right_adjust','0')->save();
		if(!isset($conf->top_bottom_adjust))$conf->updateConfig('top_bottom_adjust','-45')->save();
		if(!isset($conf->color))$conf->updateConfig('color','default')->save();
        $the_page = get_page_by_title($conf->bookmark_list_page);

        if (!$the_page){
            $_p = array();
            $_p['post_title']     = $conf->bookmark_list_page;
            $_p['post_content']   = "[binnash_bookmark_list].";
            $_p['post_status']    = 'publish';
            $_p['post_type']      = 'page';
            $_p['comment_status'] = 'closed';
            $_p['ping_status']    = 'closed';
            $_p['post_category'] = array(1);
            $conf->page_id = wp_insert_post($_p);
        }
        /*else{
            $conf->page_id = $the_page->ID;
            $the_page->post_status = 'publish';
            $conf->page_id = wp_update_post($the_page);
        }*/
        $mail_body = "Dear {first_name} {last_name},
Thank you for staying with us!
We have published a new article that may interest you.
                      
Thank You!";
        $mail_subject = "new article published.";
        $mail_signature = "Sincerely,\n" . get_bloginfo('name');
        
       // if(!isset($conf->queue))
            $conf->updateConfig('mail_body', $mail_body)
                     ->updateConfig('mail_subject', $mail_subject)
                     ->updateConfig('mail_signature', $mail_signature)
                     ->updateConfig('mail_address1', "")
                     ->updateConfig('mail_address2', "")
                     ->updateConfig('mail_city', "")
                     ->updateConfig('mail_state', "")
                     ->updateConfig('mail_zip', "")
                     ->updateConfig('queue', array())
                     ->save();        
    }
    
    static function deactivate(){
        
    }
}
}
