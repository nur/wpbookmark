<?php 
if(!class_exists('WP_List_Table')) :
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
endif;
class BinnashWpBookmarkList extends WP_List_Table{
    public function __construct(){
        parent::__construct(array(
            'singular'=>'Top Bookmarked',
            'plural'=>'Top Bookmarked',
            'ajax'=>false
        ));       
    }
    public function get_columns(){
        return array(
            'cb'=>'<input type="checkbox" />',
            'post_title'=>'Post Title',
            'post_date'=>'Post Date',
            'count'=>'Count'
        );
    }
    public function get_sortable_columns(){
        return array(
            'title'=>array('title', false)
        );
    }
    public function get_bulk_actions(){
        return array(
            'bulk_mail' => 'Send Email'
        );
    }
    public function column_default($item, $column_name){
        return $item[$column_name];
    }
    public function column_cb($item){
        return sprintf('<input type="checkbox" name="post_ids[]" value="%s"/>',$item['post_id']);
    }
    public function column_post_title($item){
        $actions = array(
            'send_email'=>sprintf('<a href="admin.php?page=%s&action=send_mail&menu_id=manage&post_id=%s" >Send Email</a>',$_REQUEST['page'],$item['post_id'])
        );    
        return $item['post_title'] . $this->row_actions($actions);
    }
    public function prepare_items(){
        global $wpdb;
        $conf = WPBookmarkConfig::getInstance();
        $query = "SELECT  post_title,post_date,COUNT(post_id) as count,post_id FROM " . $conf->bookmarks_tbl . "
                  LEFT JOIN " . $wpdb->posts . "  ON ( ".$wpdb->posts.".ID = post_id ) 
                  GROUP BY post_id 
                  ORDER BY COUNT(post_id) DESC";
        /*$query .= isset($_POST['s'])?" WHERE title LIKE '%" . 
                  $_POST['s'] . "%'": "";*/
         
        $perpage = 20;
        $paged = !empty($_GET['paged'])? mysql_real_escape_string($_GET['paged']): "";
        if(empty($paged)|| !is_numeric ($paged) || $paged <0) $paged =1;
        $totalitems = $wpdb->query($query);      
        $totalpages = ceil($totalitems)/$perpage;
        if (!empty($paged) && !empty($perpage)){
            $offset = ($paged -1) * $perpage;
            $query .= " LIMIT " . (int)$offset . ',' . (int)$perpage;    
        }
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page"    => $perpage
        ));
        $columns  = $this->get_columns();
        $hidden   = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $wpdb->get_results($query, ARRAY_A); 
    }
    public function no_items(){
       _e('No Bookmark Found.'); 
    }
}
