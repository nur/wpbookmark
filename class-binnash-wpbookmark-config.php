<?php
/**
 * Description of class-binnash-config
 *
 * @author nur
 */
class WPBookmarkConfig {
    static $_this;
    private function __construct(){
        global $wpdb;
        $this->bookmarks_tbl = $wpdb->prefix . 'wpbinnashbookmarks';
        $this->bookmark_list_page = BBOOKMARK_LIST_TITLE;
        $this->options = get_option('wpbinnashbookmarks_options');
        if(!empty($this->options))
        foreach($this->options as $key=>$value)$this->$key = $value;
    }
    public static function getInstance(){
        if(null === self::$_this){
            self::$_this = new WPBookmarkConfig();        
        }        
        return self::$_this;
    }
    public function updateConfig($options, $value=""){
	    if(is_string($options)){ 
			$this->options[$options] = $value;
			$this->$options = $value;			
		}
		else{
			foreach ($options as $key => $value) {
				$this->options[$key] = $value;
				$this->$key = $value;
			}
		}
		return $this;        
    }
	public function save(){
		update_option('wpbinnashbookmarks_options', $this->options);
	}
    public static function menu(){
        $urlPrefix = 'admin.php?page=wp_bookmark_manage&menu_id=';
        return array(
            'manage'=>array('title'=>'Manage',
                            'link'=>$urlPrefix.'manage'
                           ),
            'settings'=>array('title'=>'Settings',
                              'link'=>$urlPrefix.'settings')
        );
    }
}
