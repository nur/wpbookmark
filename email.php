<style>
.form-table input[type="text"],
.form-table select,
.form-table input[type="checkbox"],
.form-table textarea{
	width:30%;
}
.form-table input[type="radio"]{
	width:10px;
}
#binnash-email-shortcode{}
#binnash-email-shortcode span{
	text-decoration:underline;
	cursor:pointer;
} 
</style>
<div class="wrap">
<h2>Send Email<a href="admin.php?page=wp_bookmark_manage&menu_id=manage" class="add-new-h2">Back</a></h2>
<div id="message" class="updated below-h2"><?php echo isset($msg)?$msg:"";?></div>

    <form method="post">
        <input type="hidden" name="post_id" value="<?php echo $post_id;?>" />
		<table class="form-table">
			<tr class="form-field form-required">
				<th scope="row"><label for="subject"><?php _e('Subject'); ?> </label></th>
				<td><input  name="subject" type="text" id="subject" value="<?php echo $settings->mail_subject;?>" aria-required="true" /></td>
			</tr>
			<!--<tr class="form-field form-required">
				<th scope="row"><label for="send_as"><?php _e('Send As'); ?> </label></th>
				<td><input  name="send_as" checked="checked" type="radio" id="send_as" value="text" aria-required="true" />Text
				<input  name="send_as" type="radio" id="send_as" value="html" aria-required="true" />HTML</td>
			</tr> -->			
			<tr class="form-field form-required">
				<th scope="row"><label for="body"><?php _e('Message'); ?> </label></th>
				<td><textarea  name="body" id="body" aria-required="true" rows="5" cols="20" ><?php echo $settings->mail_body;?></textarea></td>
			</tr>
			<tr>
			<tr class="form-field form-required">
				<th scope="row"><label for="body"><?php _e('Shortcodes'); ?> </label></th>
				<td><div id="binnash-email-shortcode">
				<span field="user_login">User Login</span>
				<span field="user_firstname">First Name</span>
				<span field="user_lastname">Last Name</span>
				<span field="user_email">Email</span>
				<span field="ID">User ID</span>
				<span field="display_name">Display Name</span>
				<span field="user_url">User URL</span>                
				</div><p>You choose shortcodes from the list below. Each shortcode will be replaced<br/> by respective user information stored in wordpress before sending the mail.</p></td>
			</tr>			
			</tr>	
			<tr class="form-field form-required">
				<th scope="row"><label for="signature"><?php _e('Signature'); ?> </label></th>
				<td><textarea  name="signature" id="signature" aria-required="true" rows="5" cols="20" ><?php echo $settings->mail_signature;?></textarea></td>
			</tr>						
			<tr class="form-field form-required">
				<th scope="row"><label for="send_date"><?php _e('Send Date'); ?> </label></th>
				<td><input  name="send_date" type="date" id="send_date" value="<?php echo date('Y-m-d');?>" style="width:29%;" aria-required="true" /></td>
			</tr>
			<tr class="form-field form-required">
				<th scope="row">We highly recommend that you include physical address to this email to prevent it from being marked as SPAM</th>
				<td><label for="address1"><?php _e('Address Line 1'); ?> </label><br/>
                <input  name="address1" type="text" id="address1" value="<?php echo $settings->mail_address1;?>" aria-required="true" /><br/>
				<label for="address2"><?php _e('Address Line 2'); ?> </label><br/>
				<input  name="address2" type="text" id="address2" value="<?php echo $settings->mail_address2;?>" aria-required="true" /><br/>
				<label for="city"><?php _e('City'); ?> </label><br/>
				<input  name="city" type="text" id="city" value="<?php echo $settings->mail_city;?>" aria-required="true" /><br/>
				<label for="state"><?php _e('State'); ?> </label><br/>
				<input  name="state" type="text" id="state" value="<?php echo $settings->mail_state;?>" aria-required="true" /><br/>
				<label for="zip"><?php _e('Zip'); ?> </label><br/>
				<input  name="zip" type="text" id="zip" value="<?php echo $settings->mail_zip;?>" aria-required="true" /></td>
			</tr>
			<tr class="form-field form-required"> 
				<th scope="row"><label for="enqueue"><?php _e('Schedule it to send later?'); ?> </label></th>
				<td><input name="enqueue" type="checkbox" id="enqueue" value="enqueue" aria-required="true" /><p>Some mail servers may not allow sending too many mails at a time.<br/> In such case, its better to enqueue the mails and let wpbookmark choose a better time to send them.</p></td>
			</tr>						
		</table>
		<?php submit_button( __( 'Send'), 'primary', 'send_mail_button', true, array( 'id' => 'send_mail_button' ) ); ?>
    </form>
</div>
<script>
jQuery(document).ready(function($){
	$("#send_date").dateinput({'format':'yyyy-mm-dd',selectors: true,yearRange:[-100,100]});
	$('#binnash-email-shortcode>span').click(function(e){		
		$('#body').val($('#body').val()+ ' {' +$(e.target).attr('field') + '}' );
	});
});
</script>
